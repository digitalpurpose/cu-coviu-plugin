# Iframe pre and post-call message event emitter

>Note: This is addon is intended to be used when a Coviu session is embedded in an iframe

Inserts a view before and after the coviu call which emits a message event to indicate that a call has started and ended.

### Message Structure
The structure of the message event is outlined below:
```
{
    type: string; // "coviu-event"
    event: string; // "call-started" | "call-ended"
}
```

### Listening for messages
To listen for these events, an event listener will need to be set up on the parent webpage (that contains the iframe with the coviu session):
```
if (window.addEventListener) {
    window.addEventListener("message", handleMessage, false);
} else if (window.attachEvent) {
    // Support older browsers 
    window.attachEvent("onmessage", handleMessage, false);
}

function handleMessage(event) {
    // Filter out any events that aren't intended to be handled
    if (event.data.type == "coviu-event") {
        // Perform logic based on event type ...
        switch(event.data.event) {
            case "call-started":
                // ...
                break;
            
            case "call-ended":
                // ...
                break;
        }       
    }
}
```
