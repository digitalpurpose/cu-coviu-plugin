module.exports = (api) => {

    const { h: renderHtml } = api.render;

    return {
        name: 'call-started',
        phase: 'precall',
        order: 1000, // Ensure the view is always right before the call view

        required: (context) => {
            return true;
        },

        /**
         This is called when this view becomes the active view
         **/
        view: (viewApi) => {
            setTimeout(() => {
                window.parent.postMessage({
                    type: 'coviu-event',
                    event: 'call-started'
                }, "*");
            });
            viewApi.next();

            return () => {
                return renderHtml('div');
            }
        }
    }
}
