var Promise = require('es6-promise').Promise;
const precallView = require('./lib/views/pre-call');
const postcallView = require('./lib/views/post-call');

function plugin(api) {
	return Promise.all([
	]).then(function() {
		/* Register a pre-call view which emits a post message to the [cu]health system to indicate a call has begun */
		api.views.register(precallView(api));
		/* Register a post-call view which emits a post message to the [cu]health system to indicate a call has finished */
		api.views.register(postcallView(api));

		return {
			name: 'Iframe pre and post-call message event emitter'
		};
	});
}

if (typeof activate !== 'undefined') {
	activate(plugin);
} else {
	module.exports = plugin;
}
